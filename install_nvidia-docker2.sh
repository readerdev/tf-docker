# add (stable) repository and the GPG key
distribution=$(. /etc/os-release;echo $ID$VERSION_ID) \
   && curl -s -L https://nvidia.github.io/nvidia-docker/gpgkey | sudo apt-key add - \
   && curl -s -L https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.list | sudo tee /etc/apt/sources.list.d/nvidia-docker.list

# install the nvidia-docker2 package
sudo apt-get update
sudo apt-get install nvidia-docker2

# restart the Docker daemon
sudo systemctl restart docker

# verify by running a base CUDA container
docker run --rm --gpus all nvidia/cuda:11.0-base nvidia-smi
