# TensorFlow Docker Containers

This repository contains setup scripts and commands to install Docker, the NVIDIA Container Toolkit, and run TensorFlow inside a GPU-enabled container. 

## Prerequisites (Ubuntu)

### Docker engine 

First check the host system is running a supported version of Ubuntu (see [here](https://docs.docker.com/engine/install/ubuntu/) for details). Then, run the convenience script: 

```
$ sh install-docker.sh
```

This will install the Docker engine using the `apt-get` package installer and verify it with the `hello-world` image. 

### Configure Docker 

To run Docker without `sudo` privileges, follow the below post-installation steps:

1. Check if a `docker` group already exists:

```
$ cat /etc/group | grep docker
```

2. If not, create one:

```
$ sudo groupadd docker
```

3. Add the current user:

```
$ sudo usermod -aG docker $USER
```

4. Enact changes and verify:

```
$ newgrp docker 
$ docker run hello-world
```

### NVIDIA Container Toolkit 

First check the right NVIDIA driver is installed on the host system (installed and available drivers can be found in the 'Software & Updates' app on Ubuntu). Then, run the convenience script:

```
$ sh install_nvidia-docker2.sh
```

This will also verify the installation by running the `nvidia-smi` base image.  

## Run a TensorFlow container

### Base container

You can now download and start a TensorFlow container (available images can be found here   [here](https://hub.docker.com/r/tensorflow/tensorflow/) ):

```
$ docker pull tensorflow/tensorflow:2.4.1-gpu
$ docker run --gpus all --rm -it tensorflow/tensorflow:2.4.1-gpu
```

Let's check we can train a model using a fresh container. From the root of this repository, run:

```
$ docker run --gpus all --rm -it -v $(pwd):/home/tf tensorflow/tensorflow:2.4.1-gpu python /home/tf/hello_tensorflow2.py 
```

This will train and evaluate a simple CNN on the CIFAR10 dataset.

### TensorFlow Object Detection API

Included in this repository is a custom written Dockerfile for the TensorFlow 2 Object Detection API. This can be adapated for older versions of TensorFlow and the Object Detection API (e.g., API versions built on top of TensorFlow 1.x).

1. Clone a local version of `models` ([official repository](https://github.com/tensorflow/models))

2. Copy the Dockerfile in this repository to your installation of `models`

3. From the library root, build the container:

```
$ docker build -t models:2.4.1-gpu .
```

Other commands related to using the Object Detection API can be found in `models-container.txt`.
