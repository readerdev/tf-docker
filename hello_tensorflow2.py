import tensorflow as tf
from tensorflow.keras import datasets, layers, models

def check_gpu():
    """
    Note: TensorFlow runs on a GPU by default if it detects one.
    """
    gpu = len(tf.config.experimental.list_physical_devices('GPU'))
    assert (gpu == 1), 'A GPU has not been identified'

def train_cnn():
    """
    Trains and evaluates a simple CNN.

    Modified from: https://www.tensorflow.org/tutorials/images/cnn
    """
    (train_images, train_labels), (test_images, test_labels) = datasets.cifar10.load_data()

    # Normalize pixel values to be between 0 and 1
    train_images, test_images = train_images / 255.0, test_images / 255.0

    # Create the convolutional base 
    model = models.Sequential()
    model.add(layers.Conv2D(32, (3, 3), activation='relu', input_shape=(32, 32, 3)))
    model.add(layers.MaxPooling2D((2, 2)))
    model.add(layers.Conv2D(64, (3, 3), activation='relu'))
    model.add(layers.MaxPooling2D((2, 2)))
    model.add(layers.Conv2D(64, (3, 3), activation='relu'))

    # Add dense layers on top
    model.add(layers.Flatten())
    model.add(layers.Dense(64, activation='relu'))
    model.add(layers.Dense(10))

    # Compile and train the models
    model.compile(optimizer='adam',
              loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
              metrics=['accuracy'])

    history = model.fit(train_images, train_labels, epochs=10, 
                        validation_data=(test_images, test_labels))

    # Evaluate the model 
    test_loss, test_acc = model.evaluate(test_images,  test_labels, verbose=2)

    # Check the test accuracy is over 70%
    assert (test_acc > .7), 'The model has not trained to the expected accuracy'


if __name__ == "__main__":

    # Run the checks
    print('Starting checks...')
    check_gpu()
    train_cnn()
    print('Checks complete!')
