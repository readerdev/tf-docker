## Custom Dockerfile for the Tensorflow Object Detection API ##

FROM tensorflow/tensorflow:2.4.1-gpu 

RUN python -c "import tensorflow as tf; print(f'Tensorflow version: {tf.__version__}')"

ARG DEBIAN_FRONTEND=noninteractive

# Install apt-get dependencies 
RUN apt-get update && apt-get install -y \
    python3-tk \
    libgl1-mesa-glx && rm -rf /var/lib/apt/lists/*

# Name the symlink with the suffix from tensorflow-gpu
WORKDIR /usr/local/lib/python3.6/dist-packages
RUN ln -s tensorflow_gpu-* tensorflow-$(ls -d1 tensorflow_gpu* | sed 's/tensorflow_gpu-\(.*\)/\1/')

# ^^ Installing `object_detection` attempts to install the 'tensorflow' package
# So we trick pip into thinking that the 'tensorflow' package is installed
# See question 65098672: stackoverflow.com

# Install protobuf 
RUN curl -L -O https://github.com/protocolbuffers/protobuf/releases/download/v3.11.4/protoc-3.11.4-linux-x86_64.zip && \
    unzip protoc-3.11.4-linux-x86_64.zip && \
    cp bin/protoc /usr/local/bin && \
    rm -r protoc-3.11.4-linux-x86_64.zip bin/

# Copy our local version of models into the image
WORKDIR home/tf
COPY . /home/tf/models

# Compile the protocol buffers for Python
RUN (cd /home/tf/models/research/ && protoc object_detection/protos/*.proto --python_out=.)

# Install the Object Detection API
WORKDIR /home/tf/models/research/
RUN cp object_detection/packages/tf2/setup.py .

RUN python -m pip install --upgrade pip
RUN python -m pip install .

# Confirm tensorflow hasn't been reinstalled
RUN python -c "import tensorflow as tf; print(f'Tensorflow version: {tf.__version__}')"

# Add models to our python path
ENV PYTHONPATH="/home/tf/models:$PYTHONPATH" 